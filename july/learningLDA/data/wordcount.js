var fs = require("fs");
var vm = require("vm");
var readline = require('readline');
function execute(path) {
    var code = fs.readFileSync(path, 'utf-8');
    vm.runInThisContext(code, path);
};
var exec = require('child_process').exec;
var run = function (command, callback){
    exec(command, function(error, stdout, stderr){
        if ( callback !== undefined) {
            callback(stdout, stderr);
        }
    });
};

var isChinese = function(str) {
  if (!( (str[0]>='A' && str[0] <= 'Z') ||
        (str[0]>='a' && str[0] <= 'z') ||
        (str[0]>='0' && str[0] <= '9') )) {
    return true;
  }
  return false;
}

var words = {};
var parseFile  = function(index) {
  if ( index >= 140) {
    countWords();
    return;
  }

  var targetFile = "./doc/" + index + ".txt"
  var rd = readline.createInterface({
      input: fs.createReadStream(targetFile),
      output: process.stdout,
      terminal: false
  });


  rd.on('line', function(line) {
      if ( line.length > 0 && isChinese(line)) {
        if ( words[line] === undefined ) {
          words[line] = 1;
        } else {
          words[line] = words[line] + 1;
        }
      }
  });

  rd.on('close', function() {
    parseFile(index+1);
  });
}


var countWords = function() {
  //按照大小排序
  var wordSequence = [];
  for( var i in  words) {
    wordSequence.push(i);
  }

  for(var i = 0; i < wordSequence.length; i++) {
    var maxIndex = i;
    for(var j = i+1; j < wordSequence.length; j++) {
      if ( words[ wordSequence[j] ] >  words[ wordSequence[maxIndex] ] ) {
        maxIndex = j;
      }
    }

    var temp = wordSequence[maxIndex]
    wordSequence[maxIndex] =  wordSequence[i];
    wordSequence[i] = temp;
  }

  for(var i = 0; i < wordSequence.length; i++) {
    console.log(wordSequence[i] + ":" + words[ wordSequence[i] ]);
  }

}


parseFile(0)
