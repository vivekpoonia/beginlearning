var fs = require("fs");
var vm = require("vm");
var readline = require('readline');
var http = require('http');
var qs = require('querystring');
var rss = require('./node-rss');


function execute(path) {
    var code = fs.readFileSync(path, 'utf-8');
    vm.runInThisContext(code, path);
};

var exec = require('child_process').exec;
var run = function (command, callback){
    exec(command, function(error, stdout, stderr){
        if ( callback !== undefined) {
            callback(stdout, stderr);
        }
    });
};

var targetFile = "./rsslist.txt"
var titles = [];
var rd = readline.createInterface({
    input: fs.createReadStream(targetFile),
    output: process.stdout,
    terminal: false
});
rd.on('line', function(line) {
    var title = line.split(":")
    titles.push(title[0]);
});
rd.on('close', function() {
    download(0);
});

var maxLength = 512;
var bogText = "";
var docIndex = 0;

var makebow = function(gindex, articles, start) {

  if ( start >= articles.length) {
    if ( bogText.length > 0) {
      fs.writeFileSync('./doc/' + docIndex + '.txt', bogText);
    }
    bogText = "";

    docIndex++;
    download(gindex+1);
    return;
  }

  var subText = "";
  var articlesNumber = 0;
  for (var i = start; i < articles.length; i++) {
    subText += articles[i].description;
    articlesNumber ++;
    if ( subText.length >= maxLength) {
      break;
    }
  }

  var postdata = qs.stringify({
    source: subText,
    param1: '0.7',
    param2: '0'
  });

  var options = {
    host: 'api.pullword.com',
    path: '/get.php?' + postdata
  };

  var body = "";
  var request = http.request(options, function(response) {
    //When we receive data, we want to store it in a string
    response.on('data', function (chunk) {
        body += chunk;
    });
    //On end of the request, run what we need to
    response.on('end',function() {
      //var words = body.replace(/(\r\n\r\n|\r\n|\n|\r)/gm, ":");
      if ( body.indexOf('<!DOCTYPE') === -1 ) {
        console.log(body);
        bogText += body;
      }
      makebow(gindex, articles, start + articlesNumber);
    });
  });

  request.on('error', function(e) {
    console.log('problem with request: ' + e.message);
    makebow(gindex, articles, start + articlesNumber);
  });

  request.end();
};


var download = function (index) {

  if ( index >= titles.length ) {
    return;
  }

  cmd = "wget -O /tmp/rss.txt http://www.newsmth.net/nForum/rss/board-" + titles[index];
  console.log(cmd)
  run(cmd, function(){
    cmd = "iconv -f GB2312 -t UTF8 -c /tmp/rss.txt > /tmp/rss_.txt"
    console.log(cmd)
    run(cmd, function(){
      console.log(">>>>>>>>>>> 处理文件 >>>>>>>>>>>>" + titles[index]);
      rss.parseFile('/tmp/rss_.txt', function(articles) {
        makebow(index, articles, 0);
      });
    });
  });

}
